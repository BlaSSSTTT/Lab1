window.addEventListener('load', async ()=>{
    if (navigator.serviceWorker){
        try {
            await navigator.serviceWorker.register('sm.js');
        }catch (e){
            console.log("Service work fail");
        }

    }
})




document.addEventListener("DOMContentLoaded", function() {
    const GROUPS = ["PZ-21","PZ-22","PZ-23"];
    const GENDER = ["M","F"];
    let freeID = 4;
    let Student = function () {
        this.id = null;
        this.group = -1;
        this.firstname = "";
        this.secondName = "";
        this.gender = -1;
        this.birthday = "";
        this.status = false;
    }
    function transformDateFormat(dateString) {
        let dateObject = new Date(dateString);

        // Extract day, month, and year components
        let day = dateObject.getDate();
        let month = dateObject.getMonth() + 1; // January is 0, so we add 1
        let year = dateObject.getFullYear();

        // Format the date in "DD.MM.YYYY" format
        let formattedDate = `${day < 10 ? '0' + day : day}.${month < 10 ? '0' + month : month}.${year}`;

        return formattedDate;
    }
    function transformDateFormatToISO(dateString) {
        // Split the date string into day, month, and year components
        let parts = dateString.split('.');

        // Reorder the components to YYYY-MM-DD format
        let formattedDate = parts[2] + '-' + parts[1].padStart(2, '0') + '-' + parts[0].padStart(2, '0');

        return formattedDate;
    }
    function clearModal(){
        document.getElementById("id").value = "";
        document.getElementById("group").value = "";
        document.getElementById("firstName").value = "";
        document.getElementById("secondName").value = "";
        document.getElementById("gender").value = "";
        document.getElementById("birthday").value = "";
        document.getElementById("status").checked = false;
    }
    function updateModal(student) {
        document.getElementById("id").value = student.id;
        document.getElementById("group").value = GROUPS[student.gender];
        document.getElementById("firstName").value = student.firstname;
        document.getElementById("secondName").value = student.secondName;
        document.getElementById("gender").value = GENDER[student.gender];
        document.getElementById("birthday").value = transformDateFormatToISO(student.birthday);
        document.getElementById("status").checked = student.status;
    }

    function findRowByText(searchText) {
        // Get all rows in the specified table
        const tableRows = document.querySelectorAll(`table tr`);

        // Loop through each row
        for (const row of tableRows) {
            // Get the hidden <th> element within the row
            const hiddenTh = row.querySelector('th[hidden]');
            // If hidden <th> is found and contains the searchText
            if (hiddenTh && hiddenTh.textContent.trim() === searchText) {
                return row; // Return the found row
            }
        }

        // If no row is found, return null
        return null;
    }

    function editStudent(student){
        const row = findRowByText(student.id);
        const cols = row.querySelectorAll('th');
        cols[2].textContent = GROUPS[student.group];
        cols[3].textContent = student.firstname+" "+student.secondName;
        cols[4].textContent = GENDER[student.gender];
        cols[5].textContent = transformDateFormat(student.birthday);
        if(student.status){
            cols[6].innerHTML ='<i class="bi bi-circle-fill status active"></i>'
        }else {
            cols[6].innerHTML ='<i class="bi bi-circle-fill status"></i>'
        }
    }
    let openMainModal = function (button) {
        clearModal();
        let student = new Student();
        let tr = button.target.closest('tr');
        let title = "Add student";
        if (tr !== null) {
            title = "Edit student";
            let columns = tr.querySelectorAll('th');
            let data = [];
            let isActive;
            columns.forEach(column => {
                data.push(column.textContent.trim());
                if(column.querySelector('i.status')){
                    isActive =column.querySelector('i.status').classList.contains('active');
                }
            });
            student.id = data[1];
            student.group = GROUPS.indexOf(data[2]);
            let name = data[3].split(" ");
            student.firstname = name[0];
            student.secondName = name[1];
            student.gender = GENDER.indexOf(data[4]);
            student.birthday = data[5];
            student.status = data[6];
            student.status = isActive;
            console.log(student);
            updateModal(student);
        }
        document.getElementById("modalTitle").innerText = title;
        let modal = new bootstrap.Modal(document.getElementById('addStudentForm'));

        modal.show();

    }

    document.getElementById("myForm").addEventListener("submit", function(event) {
        event.preventDefault(); // Prevent the default form submission
        let student = new Student();
        student.id = document.getElementById("id").value;
        student.group = GROUPS.indexOf(document.getElementById("group").value);
        student.firstname = document.getElementById("firstName").value;
        student.secondName = document.getElementById("secondName").value;
        student.gender = GENDER.indexOf(document.getElementById("gender").value);
        student.birthday = document.getElementById("birthday").value;
        student.status = !!document.getElementById("status").checked;

        if(student.id){
            editStudent(student);
        }else{
            addStudent(student);
        }

        let modal = bootstrap.Modal.getInstance(document.getElementById("addStudentForm"));
        modal.hide();
    });
    function addStudent(student) {
          let status;
          if(student.status) {
              status = '<i class="bi bi-circle-fill status active"></i>';
          }else {
              status = '<i class="bi bi-circle-fill status"></i>';
          }
          const newRow = document.createElement('tr');
          newRow.innerHTML =
               `<th><input type="checkbox" class="table-input"></th>
                    <th hidden>${freeID}</th>
                    <th>${GROUPS[student.group]}</th>
                    <th>${student.firstname+" "+student.secondName}</th>
                    <th>${GENDER[student.gender]}</th>
                    <th>${transformDateFormat(student.birthday)}</th>
                    <th>
                        ${status}
                    <th>
                        <div class="d-flex justify-content-center">
                            <button  class="btn">
                                <i class="bi bi-pencil-square edit-btn close-btn table-icons"></i>
                            </button>
                            <button  class="btn deleteRow">
                                <i class="bi bi-trash3 delete-btn close-btn table-icons"></i>
                            </button>
                        </div>
                    </th>
          `;
            freeID++;

                document.getElementById('studentsTable').getElementsByTagName('tbody')[0].appendChild(
                    newRow);
    }

    document.getElementById('addStudentButton').addEventListener('click', openMainModal);

    function openWarningModel(button){
        let tr = button.target.closest('tr');
        let columns = tr.querySelectorAll('th');
        let name = columns[3].textContent.trim();
        document.getElementById("idOfDelete").value = columns[1].textContent;
        document.getElementById("messageForDelete").innerText = "Are you sure you want to delete student "+name+"?";
        console.log(name);
        let modal = new bootstrap.Modal(document.getElementById("deleteWarningModal"));

        modal.show();
    }

    document.getElementById("submitDelete").addEventListener('click',function (){
        const id = document.getElementById("idOfDelete").value;
        const row = findRowByText(id);
        row.parentNode.removeChild(row);
        let modal = bootstrap.Modal.getInstance(document.getElementById("deleteWarningModal"));

        modal.hide();
    });

    document.getElementById('studentsTable').addEventListener('click', function (event) {
        if (event.target.classList.contains('edit-btn')){
            openMainModal(event);
        }
        if (event.target.classList.contains('delete-btn')) {
            openWarningModel(event);
            // let row = event.target.closest('tr');
            //
            // row.parentNode.removeChild(row);
        }
    });
});